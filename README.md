Role Name
=========

A role that installs the community version of OrientDB <https://www.orientdb.org>

Role Variables
--------------

Too many. See the defaults variables file.

Dependencies
------------

* openjdk

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
